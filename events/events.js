const eventEmitter = require('events').EventEmitter;//heredamos la clase que genera los eventos
const util = require('util');//clases util, de aqui sacamos la clase que genera la herencia

var Persona = function(nombre){
    this.nombre = nombre;
}

util.inherits(Persona, eventEmitter);//se anade la herencia

let persona = new Persona('Bob');

persona.on('hablar', (mensaje) => { //se conigura el evento
    console.log(`${persona.nombre}: ${mensaje}`);
});

persona.emit('hablar', 'Hoy es una gran día');//se llama el evento